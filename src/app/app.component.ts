import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AdMobFree, AdMobFreeBannerConfig, AdMobFreeInterstitialConfig } from '@ionic-native/admob-free';

import { TabsPage } from '../pages/tabs/tabs';

import firebase from 'firebase';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
 rootPage:any;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,  public admob: AdMobFree

  ) {



       firebase.initializeApp({
  apiKey: "AIzaSyCIdsIv_tHMA5F2DV4oZgN1gDwRWUaBsbc",
    authDomain: "madeeasyrough-1483977913225.firebaseapp.com",
    databaseURL: "https://madeeasyrough-1483977913225.firebaseio.com",
    projectId: "madeeasyrough-1483977913225",
    storageBucket: "madeeasyrough-1483977913225.appspot.com",
    messagingSenderId: "762082224219"
  
  
  });
  
   

    const unsubscribe = firebase.auth().onAuthStateChanged((user) => {
 
        this.rootPage = TabsPage;
        unsubscribe();
    
    });

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

       let bannerConfig: AdMobFreeBannerConfig = {
           // isTesting: true, // Remove in production
            autoShow: true,
            id:'ca-app-pub-4607892206659507/8614520377'
      };

      this.admob.banner.config(bannerConfig);

      this.admob.banner.prepare().then(() => {
          // success
      }).catch(e => console.log(e));
    });
  }
}

  
  

    


